
public class Client {

	public static void main(String[] args) {
		System.out.println("Implementing Heaps");
		
		int [] list = {10,2,12,22,7,20};
		
		Heap h1 = new Heap();
		
		for(Integer i : list)
			h1.insert(i);
		h1.displayHeap();
		//h1.display();
	}

}
