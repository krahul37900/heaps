import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Heap {
	private ArrayList<Integer> array;
	
	/**
	 * Construct a new heap.
	 */
	public Heap()
	{
		array = new ArrayList<Integer>();
	}
	
	/**
	 * Insert an element into the heap
	 * @param element
	 */
	public void insert(int element)
	{
		if(!array.isEmpty()) {
			array.add(element);
			heapify(array.size()-1);
		}
		else
			array.add(element);
		
	} 

	/**
	 * Maintain the heap property.
	 * @param index
	 */
	private void heapify(int index) {
		//System.out.println("Index = " + index);
		int parentIndex = getParent(index);
		if(array.get(parentIndex) > array.get(index))
		{
			swap(parentIndex, index);
			heapify(parentIndex);
		}
	}

	/**
	 * Level Order Traversal of Heap
	 */
	public void displayHeap()
	{
		if(!array.isEmpty())
		{
			Queue<Integer> queue = new LinkedList<Integer>();
			queue.add(0);
			int numElePrinted = 0;
			int level = 1;
			System.out.println(array.get(0));
			while(!queue.isEmpty())
			{
				int index = queue.remove();
				int leftIndex = 2*index+1;
				int rightIndex = 2*index+2;
				if(leftIndex < array.size())
				{
					System.out.print(array.get(leftIndex) + " ");
					numElePrinted++;
					queue.add(leftIndex);
				}
				if(rightIndex < array.size())
				{
					System.out.print(array.get(rightIndex)+ " ");
					numElePrinted++;
					queue.add(rightIndex);
				}
				
				if(numElePrinted >= (1<<level))
				{
					System.out.println();
					numElePrinted = 0;
					level++;
				}
			}
		} else {
			System.out.println("Heap is empty boss!");
		}
		
	}
	
	/**
	 * Print out the array 
	 */
	public void display()
	{
		for(Integer i : array)
			System.out.print(i+" ");
		System.out.println();
	}
	
	/**
	 * Swap parentIndex and index
	 * @param parentIndex
	 * @param index
	 */
	private void swap(int parentIndex, int index) {
		int temp = array.get(parentIndex);
		array.set(parentIndex, array.get(index));
		array.set(index, temp);
	}

	/**
	 * Returns the parent of the current index
	 * @param index
	 * @return
	 */
	private int getParent(int index) {
		return index/2;
	} 
}
